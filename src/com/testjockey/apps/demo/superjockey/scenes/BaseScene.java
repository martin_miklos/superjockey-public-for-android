package com.testjockey.apps.demo.superjockey.scenes;

import hu.virgo.testjockeysdk.core.TestJockey;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.util.ScreenGrabber;
import org.andengine.extension.debugdraw.DebugRenderer;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.math.MathUtils;

import superjockey.TJIllBeBackException;
import superjockey.TJImYourFatherException;
import superjockey.TJResistanceIsFutileYouWillBeAssimilatedException;
import superjockey.TJTheLifeTheUniverseAndEverythingException;
import superjockey.TJThereIsNoFateButWhatWeMakeException;
import superjockey.TJWhatBehindTheRabbitException;
import superjockey.TJYouShallNotPassException;
import android.graphics.Bitmap;
import android.graphics.Matrix;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.R;
import com.testjockey.apps.demo.superjockey.gameelements.Obstacle;
import com.testjockey.apps.demo.superjockey.helpers.GameManager;
import com.testjockey.apps.demo.superjockey.helpers.ObstacleFactory;
import com.testjockey.apps.demo.superjockey.helpers.ResourceManager;
import com.testjockey.apps.demo.superjockey.pojo.Player.SJCharacterName;

public abstract class BaseScene extends Scene implements ContactListener, IOnSceneTouchListener {

	enum State {
		NEW, 
		PAUSED, 
		PLAY, 
		DEAD, 
		AFTERLIFE;
	}
	
	protected PhysicsWorld physics;
	
	protected Body heroBody;
	
	protected State state = State.NEW;
	protected State lastState = state;	
	
	protected Text scoreText;
	private Text restartText;
	private Text menuText;
	
	protected HUD hud = new HUD();
	
	protected TiledSprite hero;
	
	protected ResourceManager resourceManager = ResourceManager.getInstance();
	protected VertexBufferObjectManager vbom = ResourceManager.getInstance().vbom;
	protected GameManager gameManager = GameManager.getInstance();	
	
	protected LinkedList<Sprite> backgrounds = new LinkedList<Sprite>();
	protected LinkedList<Obstacle> obstacles = new LinkedList<Obstacle>();
	protected LinkedList<Sprite> bottoms = new LinkedList<Sprite>();
	protected LinkedList<Rectangle> tops = new LinkedList<Rectangle>();
	protected LinkedList<TiledSprite> stars = new LinkedList<TiledSprite>();
	protected LinkedList<Body> starsBodies = new LinkedList<Body>();
	protected LinkedList<TiledSprite> lives = new LinkedList<TiledSprite>();
	
	protected int difficultyMultiplier = 1;
	protected float heroXSpeed = Constants.SPEED_X;	
	protected int score, starScore = 0;
	protected long timestamp = 0;
	protected boolean sensorLeaved = false;
	
	private int scoreToPinFlag = Constants.TJ_SCORE_TO_PIN;
	private boolean newHighScore;
	
	ScreenGrabber grabber;
	
	public BaseScene() {
		commonInit();
	}
	
	public BaseScene(int livesCount, float currentSpeed, int score, int starScore, State currentState, int heroYPos, float yImpulse) {
		destroyAllStar();
		destroyAllObstacle();

		this.heroXSpeed = currentSpeed;		
		
		commonInit();			
		
		this.score = score;
		this.starScore = starScore;
		refreshScoreText();		
		
		initHeroPosition(heroYPos);
		
		for (int i = 0; i < livesCount; i++) {
			createHeart();
		}
		this.state = currentState;
		if (currentState == State.AFTERLIFE) {
			restartText.setVisible(true);
			menuText.setVisible(true);
		}

		physics.setGravity(new Vector2(0, Constants.GRAVITY));
		heroBody.setLinearVelocity(new Vector2(heroXSpeed, 0));
		heroBody.applyLinearImpulse(new Vector2(0, yImpulse), heroBody.getPosition());
	}
	
	private void commonInit() {		
		physics = new PhysicsWorld(new Vector2(0, 0), true);
		physics.setContactListener(this);
		ObstacleFactory.getInstance().create(physics);
		
		ObstacleFactory.getInstance().increaseObstacleDistanceBySpeed(heroXSpeed);
		
		setOnSceneTouchListener(this);
		registerUpdateHandler(physics);
		
		addObstacle();
		addObstacle();
		addObstacle();
		
		createBackground();
		createBounds();
		createActor();
		createText();
		
		resourceManager.camera.setChaseEntity(hero);
		
		sortChildren();

		if (Constants.DEBUG_MODE) {
			DebugRenderer debug = new DebugRenderer(physics, vbom);
			attachChild(debug);	
		}
		
		gameManager.fillUpGameManager(resourceManager.activity);
		switch (gameManager.getDifficulty()) {
			case Easy:
				this.difficultyMultiplier = 1;
				break;
			case Medium:
				this.difficultyMultiplier = 2;
				break;
			case Hard:
				this.difficultyMultiplier = 4;
				break;
		}
		
		restartText = new Text(resourceManager.camera.getWidth() / 2 , resourceManager.camera.getHeight() / 1.50f, resourceManager.font, "RESTART", vbom);
		hud.attachChild(restartText);
		menuText = new Text(resourceManager.camera.getWidth() / 2 , resourceManager.camera.getHeight() / 1.80f, resourceManager.font, "MENU", vbom);
		hud.attachChild(menuText);
		restartText.setVisible(false);
		menuText.setVisible(false);
		
		grabber = new ScreenGrabber();
		grabber.setZIndex(1000000);
		attachChild(grabber);
	}
	
	public void resume() {
		resourceManager.bgMusic.play();
	}
	
	public void pause() {
		resourceManager.bgMusic.pause();
		unregisterUpdateHandler(physics);
	}
	
	private void createBackground() {
		Sprite firstBg = createBg(0); 
		backgrounds.add(firstBg);
		backgrounds.add(createBg(firstBg.getX() + firstBg.getWidth()));
	}
	
	protected abstract Sprite createBg(float posX);
	
	private void createBounds() {
		Sprite firstBottom = createBottom(0, Constants.BOTTOM_Y_POSITION); 
		bottoms.add(firstBottom);
		bottoms.add(createBottom(firstBottom.getX() + firstBottom.getWidth(), Constants.BOTTOM_Y_POSITION));
		
		Rectangle firstTop = createTop(0);
		tops.add(firstTop);
		tops.add(createTop(firstTop.getX() + firstTop.getWidth()));
	}
	
	protected abstract Rectangle createTop(float posX);
	
	protected abstract Sprite createBottom(float posX, float posY);
	
	private void createActor() {
		if (gameManager.getPlayer().getSelectedCharacter() == SJCharacterName.SJSuzy) {
			hero = new TiledSprite(200, 400, resourceManager.suzyRegion, vbom);
		} else {
			hero = new TiledSprite(200, 400, resourceManager.jackRegion, vbom);
		}
		hero.setZIndex(999);
		hero.registerUpdateHandler(new IUpdateHandler() {

		float heroAnimationUpdateTime = 0;
			
			@Override
			public void onUpdate(float pSecondsElapsed) {

				if (state == State.PLAY) {
					heroAnimationUpdateTime += pSecondsElapsed;
					if (heroAnimationUpdateTime > 0.2) {
						switch (hero.getCurrentTileIndex()) {
							case 0:
								hero.setCurrentTileIndex(1);
								break;
							case 1:
								hero.setCurrentTileIndex(0);
								break;
						}
						heroAnimationUpdateTime = 0;
					}
				}
			}

			@Override
			public void reset() { }
		});
/*		
		BodyDef bd = new BodyDef();
	    bd.position.set(0.5f, 0.5f);
	    bd.type = BodyType.DynamicBody;
		
		heroBody = physics.createBody(bd);
		
		BodyEditorLoader bel = new BodyEditorLoader(loadJSONFromAsset("bodies/hero.json"));
		bel.attachFixture(heroBody, "hero", Constants.FIXTURE_HERO, 3.5f);
*/
		heroBody = PhysicsFactory.createCircleBody(
				physics, hero.getX() + hero.getWidth() / 2, hero.getY() + hero.getHeight() / 2, 50, BodyType.DynamicBody, Constants.FIXTURE_HERO);
		heroBody.setUserData(Constants.BODY_HERO);
		physics.registerPhysicsConnector(new PhysicsConnector(hero, heroBody));
		attachChild(hero);
	}

	
	private void createText() {
		resourceManager.camera.setHUD(hud);
		
		scoreText = new Text(resourceManager.camera.getWidth() / 2 , resourceManager.camera.getHeight() / 1.25f, resourceManager.font, "12345678901234567890", vbom);
		hud.attachChild(scoreText);
	}
	
	protected void destroyStar(TiledSprite star, Body starBody) {
		detachChild(star);
		starBody.setActive(false);
		physics.destroyBody(starBody);
		star = null;
		starBody = null;
	}
	
	abstract protected void addObstacle();
	
	protected abstract void createStar(float posX, float posY);
	
	protected void createStar(float posX, float posY, TiledTextureRegion starRegion) {
		final TiledSprite starSprite = new TiledSprite(posX, posY, starRegion, vbom);
		stars.add(starSprite);
		
		starSprite.setZIndex(100);
		starSprite.registerUpdateHandler(new IUpdateHandler() {
		float starAnimationUpdateTime = 0;
			@Override
			public void reset() {
			}
			
			@Override
			public void onUpdate(float pSecondsElapsed) {
				starAnimationUpdateTime += pSecondsElapsed;
				if (starAnimationUpdateTime > 0.15) {
					switch (starSprite.getCurrentTileIndex()) {
					case 0:
						starSprite.setCurrentTileIndex(3);
						break;
					case 1:
						starSprite.setCurrentTileIndex(2);
						break;
					case 2:
						starSprite.setCurrentTileIndex(0);
						break;
					case 3:
						starSprite.setCurrentTileIndex(1);
						break;
					}
					starAnimationUpdateTime = 0;
				}
			}
		});
		
		attachChild(starSprite);
		Body starBody = PhysicsFactory.createCircleBody(physics, starSprite, BodyType.StaticBody, Constants.FIXTURE_STAR);
		starBody.setUserData(Constants.BODY_STAR);
		starsBodies.add(starBody);
	}
	
	protected void createHeart() {
		float posX;
		float posY = resourceManager.camera.getHeight() - resourceManager.heartRegion.getHeight();
		if (lives.size() == 0) {
			posX = resourceManager.camera.getWidth() - resourceManager.heartRegion.getWidth();
		} else {
			posX = lives.getLast().getX() - resourceManager.heartRegion.getWidth();
		}
		final TiledSprite heartSprite = new TiledSprite(posX, posY, resourceManager.heartRegion, vbom);
		heartSprite.setZIndex(10);
		heartSprite.registerUpdateHandler(new IUpdateHandler() {
			float heartAnimationUpdateTime = 0;			
			@Override
			public void reset() {
			}
			
			@Override
			public void onUpdate(float pSecondsElapsed) {
				heartAnimationUpdateTime += pSecondsElapsed;
				if (heartAnimationUpdateTime > 0.3) {
					switch (heartSprite.getCurrentTileIndex()) {
					case 0:
						heartSprite.setCurrentTileIndex(1);
						break;
					case 1:
						heartSprite.setCurrentTileIndex(0);
						break;
					}
					heartAnimationUpdateTime = 0;
				}
			}
		});
		
		hud.attachChild(heartSprite);
		lives.add(heartSprite);
	}
	
	private void lostLife() {
		playLostLifeSound();
		TiledSprite lastHeart = lives.getLast(); 
		lastHeart.detachSelf();
		lives.remove(lastHeart);
	}
	
	private void playStarCollectedSound() {
		if (gameManager.isMusicOn()) {
			resourceManager.collectStarSound.play();
		}
	}
	
	private void playNewLifeSound() {
		if (gameManager.isMusicOn()) {
			resourceManager.newLifeSound.play();
		}
	}
	
	private void playLostLifeSound() {
		if (gameManager.isMusicOn()) {
			resourceManager.lostLifeSound.play();
		}
	}
	
	private void playDieSound() {
		if (gameManager.isMusicOn()) {
			resourceManager.dieSound.play();
		}
	}
	
	protected void sendScoreFlags() {
		int overAllScore = getOverallScore();
		
		if (overAllScore >= scoreToPinFlag && overAllScore > 0) {
			TestJockey.pinFlag(TestJockey.Flag.builder(scoreToPinFlag + Constants.TJ_FLAG_POINT_REACHED).requestTelemetry(true).build());
			scoreToPinFlag += Constants.TJ_SCORE_TO_PIN;				
		}

		if (overAllScore > gameManager.getHighScore()) {
			if (!newHighScore) {
				grabber.grab((int)resourceManager.activity.getWindowManager().getDefaultDisplay().getWidth(), (int)resourceManager.activity.getWindowManager().getDefaultDisplay().getHeight() , 
						new ScreenGrabber.IScreenGrabberCallback() {
					
					@Override
					public void onScreenGrabbed(Bitmap pBitmap) {
						TestJockey.pinFlag(TestJockey.Flag.builder(Constants.TJ_FLAG_HISCORE).requestTelemetry(true).requestScreenshot(resourceManager.activity.surfaceView, flipVertical(pBitmap)).build());
					}
					
					@Override
					public void onScreenGrabFailed(Exception pException) {
						pException.printStackTrace();
					}
				});
				
				newHighScore = true;
			}
		}
	}
	
	protected void refreshScoreText() {
		scoreText.setText(String.valueOf(getOverallScore()));
	}
	
	protected int getOverallScore() {
		return score + starScore * Constants.STAR_MULTIPLIER;
	}
	
	private float clamp(float val, float min, float max) {
		return Math.max(min, Math.min(max, val));
	}
	
	private void increaseSpeedAndObstaclesDistance() {
		sortChildren();
		if (score % 3 == 0 && score > 0) {
			heroXSpeed += 1.1f;
			ObstacleFactory.getInstance().increaseObstacleDistanceBySpeed(heroXSpeed);
		}
	}
	
	protected abstract void repeatLvlBackground();
	
	protected abstract void repeatBottom();
	
	protected abstract void increaseStarScore();

	@Override
	public void reset() {
		destroyAllStar();
		
		newHighScore = false;
		
		heroXSpeed = Constants.SPEED_X * difficultyMultiplier;
		physics.setGravity(new Vector2(0, 0));
		
		heroBody.setLinearDamping(0);
		
		initScores();
		scoreToPinFlag = Constants.TJ_SCORE_TO_PIN;
		
		startNewGame();
		
		destroyAllObstacle();
		ObstacleFactory.getInstance().reset();
		
		initHeroPosition(400);
		
		addObstacle();
		addObstacle();
		addObstacle();

		scoreText.setText(resourceManager.activity.getString(R.string.hiscore) + gameManager.getHighScore());
		
		sortChildren();	

		unregisterPhysicsUpdate();
		
		state = State.NEW;
		
		if (lives.size() == 0) {
			createHeart();
		}
		super.reset();
	}

	private void unregisterPhysicsUpdate() {
		unregisterUpdateHandler(physics);
		physics.onUpdate(0);
	}

	private void initHeroPosition(int posY) {
		heroBody.setTransform(Constants.HERO_X_POS / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT,
				posY / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT, 0);
	}

	private void initScores() {
		score = 0;
		starScore = 0;
		refreshScoreText();
	}

	private void destroyAllObstacle() {
		for (Obstacle obstacle : obstacles) {
			obstacle.destroy();
			detachChild(obstacle);
		}
		obstacles = new LinkedList<Obstacle>();
	}

	private void destroyAllStar() {
		if (stars.size() > 0 && starsBodies.size() > 0) {
			for (int i = 0; i < stars.size(); i++) {
				destroyStar(stars.get(i), starsBodies.get(i));
			}
			stars = new LinkedList<TiledSprite>();
			starsBodies = new LinkedList<Body>();
		}
	}

	protected abstract void startNewGame();
	
	@Override
	public void beginContact(Contact contact) {
		if (Constants.BODY_OBSTACLE.equals(contact.getFixtureA().getBody().getUserData()) ||
				Constants.BODY_OBSTACLE.equals(contact.getFixtureB().getBody().getUserData())) {	
			if ((state == State.DEAD || state == State.AFTERLIFE) && hero.getY() < 300) {
				heroBody.setLinearDamping(7f);
			}
			if (lives.size() > 1) {
				lostLife();
			} else {
				if (state == State.PLAY) {
					lostLife();
					playDieSound();
					state = State.DEAD;
					
					TestJockey.pinFlag(TestJockey.Flag.builder(Constants.TJ_FLAG_GAME_OVER).requestTelemetry(true).build());
					
					gameManager.setScoreToPlayer(score, starScore * Constants.STAR_MULTIPLIER);
					resourceManager.activity.save();
					
					timestamp = System.currentTimeMillis();
					heroBody.setLinearVelocity(0, 0);
					
					for (Obstacle obstacle : obstacles) {
						obstacle.setInactive();
					}
				}
			}
		} else if (Constants.BODY_STAR.equals(contact.getFixtureA().getBody().getUserData()) ||
				Constants.BODY_STAR.equals(contact.getFixtureB().getBody().getUserData())) {
			if (state == State.PLAY) {
				TestJockey.pinFlag(TestJockey.Flag.builder(Constants.TJ_FLAG_STAR_COLLECTED).requestTelemetry(true).build());
				playStarCollectedSound();
				if (gameManager.getCrashFrequency() > 0) {	
					if (gameManager.getCrashFrequency() > Math.random()) {
						int exceptionNumber = (int)(Math.random() * 7);
						switch (exceptionNumber) {
				            case 0:
				            	throw new TJThereIsNoFateButWhatWeMakeException();
				            case 1:
				            	throw new TJWhatBehindTheRabbitException();
				            case 2:
				            	throw new TJResistanceIsFutileYouWillBeAssimilatedException();
				            case 3:
				            	throw new TJYouShallNotPassException();
				            case 4:
				            	throw new TJTheLifeTheUniverseAndEverythingException();
				            case 5:
				            	throw new TJImYourFatherException();
				            case 6:
				            	throw new TJIllBeBackException();
				        }
					}
				}
				increaseStarScore();
				
				Body aBody = contact.getFixtureA().getBody();
				Body bBody = contact.getFixtureB().getBody();
				
				int loc = 0;
				if (Constants.BODY_STAR.equals(aBody.getUserData())) {
					loc = starsBodies.indexOf(aBody);
					destroyStar(stars.get(loc), aBody);
				} else {
					loc = starsBodies.indexOf(bBody);
					destroyStar(stars.get(loc), bBody);
				}
				stars.remove(loc);
				starsBodies.remove(loc);
				if (starScore % 3 == 0) {
					createHeart();
					playNewLifeSound();
				}

			}
		}
	}

	@Override
	public void endContact(Contact contact) {
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if (pSceneTouchEvent.isActionDown()) {
			if (state == State.PAUSED) {
				if (lastState != State.NEW) {
					registerUpdateHandler(physics);
				}
				state = lastState;
			} else if (state == State.NEW) {
				startNewGame();
				registerUpdateHandler(physics);
				TestJockey.startExperiment("With_orWithoutHint", true);
				state = State.PLAY;
				physics.setGravity(new Vector2(0, Constants.GRAVITY));
				heroBody.setLinearVelocity(new Vector2(Constants.SPEED_X, 0));
			} else if (state == State.DEAD) {
				// don't touch the dead!
			} else if (state == State.AFTERLIFE) {
				// TODO: I don't know why, but it works. The 80 is a REAL magic number.
				int touchX = (int)(pSceneTouchEvent.getX() - resourceManager.camera.getXMin() + 80);
				if (touchX >= restartText.getX() && touchX <= (restartText.getX() + restartText.getWidth()) && 
						pSceneTouchEvent.getY() <= restartText.getY() && pSceneTouchEvent.getY() >= (restartText.getY() - restartText.getHeight())) {
					restartText.setVisible(false);
					menuText.setVisible(false);
					TestJockey.releaseExperiment();
					reset();
					state = State.NEW;					
				}
				if (touchX >= menuText.getX() && touchX <= (menuText.getX() + menuText.getWidth()) && 
						pSceneTouchEvent.getY() <= menuText.getY() && pSceneTouchEvent.getY() >= (menuText.getY() - menuText.getHeight())) {
					resourceManager.activity.onBackPressed();
				}
			} else {
				Vector2 v = heroBody.getLinearVelocity();
				v.x = heroXSpeed;
				v.y = Constants.SPEED_Y;
				heroBody.setLinearVelocity(v);
			}
		}
		return false;
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		if (state == State.PLAY) {
			float currentRotation = heroBody.getLinearVelocity().y * -4;
			float newRotation = clamp(currentRotation, -15, 45);
			hero.setRotation(newRotation);
			heroBody.setTransform(heroBody.getPosition(), MathUtils.degToRad(-newRotation));
		}
		
		if (sensorLeaved) {
			increaseSpeedAndObstaclesDistance();
			sensorLeaved = false;
		}

		if (!obstacles.isEmpty()) {
			Obstacle obstacle = obstacles.getFirst();
			if (obstacle.getX() + obstacle.getWidth() < resourceManager.camera.getXMin()) {
				obstacle.destroy();
				detachChild(obstacle);
				obstacles.remove();
				addObstacle();
			}
		}
		
		if (state == State.DEAD && timestamp + Constants.TIME_TO_RESSURECTION < System.currentTimeMillis()) {
			state = State.AFTERLIFE;
			restartText.setVisible(true);
			menuText.setVisible(true);
		}

		repeatLvlBackground();
		
		repeatBottom();
		
		if (!tops.isEmpty()) {
			Rectangle topFirst = tops.getFirst();
			Rectangle topSecond = tops.get(1);
			if (topFirst.getX() + topFirst.getWidth() < resourceManager.camera.getXMin()) {
				tops.remove();
				tops.add(createTop(topSecond.getX() + topSecond.getWidth()));
				topFirst = null;
			}
		}
		
		if (!stars.isEmpty()) {
			TiledSprite firstStar = stars.getFirst();
			Body firstBody = starsBodies.getFirst();
			if (firstStar.getX() + firstStar.getWidth() < resourceManager.camera.getXMin()) {
				destroyStar(firstStar, firstBody);
				stars.remove();
				starsBodies.remove();
			}
		}
		
		super.onManagedUpdate(pSecondsElapsed);
	}

	private String loadJSONFromAsset(String fileName) {
	    String json = null;
	    try {

	        InputStream is = resourceManager.activity.getAssets().open(fileName);

	        int size = is.available();

	        byte[] buffer = new byte[size];

	        is.read(buffer);

	        is.close();

	        json = new String(buffer, "UTF-8");


	    } catch (IOException ex) {
	        ex.printStackTrace();
	        return null;
	    }
	    return json;

	}
	
	private static Bitmap flipVertical(Bitmap src) {
	    Matrix matrix = new Matrix();
	    matrix.preScale(1.0f, -1.0f);
	    return Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
	}

	
}
