package com.testjockey.apps.demo.superjockey.helpers;

import org.andengine.extension.physics.box2d.PhysicsWorld;

import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.gameelements.LevelObstacle;
import com.testjockey.apps.demo.superjockey.gameelements.SpaceObstacle;

public class ObstacleFactory {
	
	private static final int MIN_Y_FOR_OBSTACLE = -170;
	private static final int RANDOM_Y_DEVIATION_FOR_OBSTACLE = 400;
	private static final ObstacleFactory INSTANCE = new ObstacleFactory();

	
	int nextX;
	int nextY;
	int dy;
	private int pipeDistance = Constants.PIPE_DISTANCE;
	private boolean pipeDistanceModified = false;
	
	final int maxY = 550;
	final int minY = 350;
	
	private PhysicsWorld physics;
	private ResourceManager resourceManager = ResourceManager.getInstance();
	
	public static final ObstacleFactory getInstance() {
		return INSTANCE;
	}
	
	public void create(final PhysicsWorld physics) {
		this.physics = physics;
		reset();
	}
	
	public LevelObstacle nextLevelObstacle(int difficulty) {
		
		if (difficulty <= 8) {
			difficulty *= 10;	
		} else {
			difficulty = 80;
		}
		
		int distanceBetweenObstacles = (int)Constants.DISTANCE_BETWEEN_OBSTACLES - difficulty;
		
		LevelObstacle obstacle = new LevelObstacle(
				nextX, 
				nextY,
				distanceBetweenObstacles,
				resourceManager.candyTopRegion,
				resourceManager.candyBottomRegion,
				resourceManager.vbom, 
				physics);
		setPosition();
		return obstacle;
	}

	private void setPosition() {
		nextX += pipeDistance;
		nextY = MIN_Y_FOR_OBSTACLE + (int)(Math.random() * RANDOM_Y_DEVIATION_FOR_OBSTACLE);
	}
	
	public SpaceObstacle nextSpaceObstacle() {
		if (!pipeDistanceModified) {
			pipeDistance += 30;
		}
		SpaceObstacle obstacle = new SpaceObstacle(
				nextX, 
				nextY, 
				resourceManager.vbom, 
				physics);
		setPosition();
		return obstacle;
	}

	public void reset() {
		nextX = 650;
		nextY = 0;
		pipeDistance = Constants.PIPE_DISTANCE;
	}
	
	public void increaseObstacleDistanceBySpeed(float speed) {
		this.pipeDistance = Constants.PIPE_DISTANCE + (int)speed * 10;
	}
	
	public int getObstacleDistance() {
		return pipeDistance;
	}
	
}
