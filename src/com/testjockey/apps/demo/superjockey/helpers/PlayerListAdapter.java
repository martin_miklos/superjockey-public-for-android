package com.testjockey.apps.demo.superjockey.helpers;

import java.util.Vector;

import com.testjockey.apps.demo.superjockey.R;
import com.testjockey.apps.demo.superjockey.pojo.Player;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PlayerListAdapter extends ArrayAdapter<Player> {

	private Context context;
	private Vector<Player> players;
	
	public PlayerListAdapter(Context context, int resource, Vector<Player> players) {
		super(context, resource);
		this.context = context;
		this.players = players;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.row_highscore, parent, false);
		TextView tvName = (TextView)rowView.findViewById(R.id.tvName);
		tvName.setText(players.get(position).getName());
		TextView tvScore = (TextView)rowView.findViewById(R.id.tvScore);
		tvScore.setText(players.get(position).getOverallPoints() + "");
		return rowView;
	}

	@Override
	public int getCount() {
		return players.size();
	}

	
	
}
