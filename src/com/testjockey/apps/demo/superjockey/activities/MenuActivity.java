package com.testjockey.apps.demo.superjockey.activities;

import hu.virgo.testjockeysdk.core.TestJockey;

import com.testjockey.apps.demo.superjockey.helpers.GameManager;
import com.testjockey.apps.demo.superjockey.pojo.Player;
import com.testjockey.apps.demo.superjockey.pojo.Player.SJCharacterName;
import com.testjockey.apps.demo.superjockey.Constants;
import com.testjockey.apps.demo.superjockey.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class MenuActivity extends Activity {

	private ImageButton ibCharacterSelection;
	private Button btStart, btSettings, btScores;
	private EditText etPlayerName;
	private MediaPlayer mediaPlayer = null;
	private GameManager gameManager = GameManager.getInstance();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        
        initMediaPlayer();
        
        ibCharacterSelection = (ImageButton)findViewById(R.id.characterChooserButton);
        etPlayerName = (EditText)findViewById(R.id.etPlayerName);
        
        gameManager.fillUpGameManager(this);
        if (gameManager.getPlayer() == null) {
        	Player player = new Player();
        	player.setSelectedCharacter(SJCharacterName.SJSuzy);
        	player.setName(getResources().getString(R.string.suzy_name));
        	gameManager.setPlayer(player);
        }
		if (gameManager.getPlayer().getSelectedCharacter() == SJCharacterName.SJSuzy) {
			ibCharacterSelection.setImageResource(R.drawable.sjsuzywithbackground);
			if (isBaseNameNeedToSet()) {
				etPlayerName.setText(R.string.suzy_name);				
			}
		} else {
			ibCharacterSelection.setImageResource(R.drawable.sjackwithbackground);
			if (isBaseNameNeedToSet()) {
				etPlayerName.setText(R.string.jack_name);				
			}
		}
        
        ibCharacterSelection.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				stopMusic();
			    startActivity(new Intent(MenuActivity.this, CharacterSelectionActivity.class));
			    finish();
			}
		});
        
        btStart = (Button)findViewById(R.id.btStart);
        btStart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				stopMusic();			
				startActivity(new Intent(MenuActivity.this, GameActivity.class));
				finish();
			}
		});
        
        btSettings = (Button)findViewById(R.id.btSettings);
        btSettings.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MenuActivity.this, SettingsActivity.class));
			}
		});
        
        btScores = (Button)findViewById(R.id.btScores);
        btScores.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MenuActivity.this, ScoresActivity.class));
			}
		});
        
        etPlayerName.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				gameManager.getPlayer().setName(s.toString());
				savePlayername();
			}
		});
        
        RelativeLayout rlHint = (RelativeLayout)findViewById(R.id.rlHint);
        
        TestJockey.startExperiment("With_orWithoutHint", true);
        if (TestJockey.isA()) {
        	rlHint.setVisibility(View.VISIBLE);
        }
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		playMusic();
	}
	
	@Override
	protected void onStop() {
		stopMusic();
		super.onStop();
	}

	@Override
	public void onBackPressed() {
		stopMusic();
	    startActivity(new Intent(MenuActivity.this, CharacterSelectionActivity.class));
	    finish();
	}
	
	private void playMusic() {
		if (gameManager.isMusicOn()) {
			if (!mediaPlayer.isPlaying()) {
				initMediaPlayer();
				mediaPlayer.start();
			}			
		} else {
			stopMusic();
		}
	}
	
	private void stopMusic() {
		if (mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
	}
	
	private void initMediaPlayer() {
        mediaPlayer = new MediaPlayer();;
        mediaPlayer = MediaPlayer.create(this, R.raw.sjmenu);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setLooping(true);
	}
	
	private boolean isBaseNameNeedToSet() {
		boolean result = true;
		if (!gameManager.getPlayer().getName().equals(getResources().getString(R.string.jack_name)) &&
				!gameManager.getPlayer().getName().equals(getResources().getString(R.string.suzy_name)) &&
				!gameManager.getPlayer().getName().equals("")) {
			etPlayerName.setText(gameManager.getPlayer().getName());
			result = false;
		}
		return result;
	}
	
	private void savePlayername() {
		SharedPreferences.Editor settingsEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
    	settingsEditor.putString(Constants.KEY_PLAYERNAME, gameManager.getPlayer().getName());
    	settingsEditor.commit();
	}
	
}
